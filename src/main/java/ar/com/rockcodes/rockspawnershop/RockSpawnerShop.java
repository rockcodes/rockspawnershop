package ar.com.rockcodes.rockspawnershop;

import java.io.File;
import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import ar.com.rockcodes.rockspawnershop.Listerner.Comandos;
import ar.com.rockcodes.rockspawnershop.Listerner.Eventos;
import ar.com.rockcodes.rockspawnershop.config.SimpleConfig;
import ar.com.rockcodes.rockspawnershop.config.SimpleConfigManager;


public class RockSpawnerShop extends JavaPlugin {
	public static RockSpawnerShop plugin;
	public SimpleConfigManager manager;
	public SimpleConfig config;
    private static final Logger log = Logger.getLogger("Minecraft");
    public static Economy econ = null;
    public static Permission perms = null;
	
	public void onEnable() {
		
		 String[] header = {"Rock-SpawnerShop", "Config"};
		 if (!setupEconomy() ) {
	            log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
	            getServer().getPluginManager().disablePlugin(this);
	            return;
	     }
	     setupPermissions();
	        
	     this.manager = new SimpleConfigManager(this);   
	     if (!new File(getDataFolder().toString() + File.separator +"config.yml").exists()){
	   	 this.config = manager.getNewConfig("config.yml", header);
		 this.config.set("Enable", true , "Activate plugin");
		 this.config.set("Spawner.Zombie.price", new Double(10000) );
		 this.config.set("Spawner.Skeleton.price", new Double(10000) );
		 this.config.set("Spawner.Spider.price", new Double(10000) );
		// this.config.set("ComandoADD", comandoadd , "Comandos al ejecutar la fila con el cmd ADD en db");
		 this.config.saveConfig();
	     }else{this.config = manager.getNewConfig("config.yml", header);}
		 
			 
		 getServer().getPluginManager().registerEvents(new Eventos(this), this);
		 getCommand("spawner").setExecutor(new Comandos(this));
		 
		
	}
	
	
	
	public void onDisable() {
		
		
	}
	
	
	private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }
	
	
}
