package ar.com.rockcodes.rockspawnershop.Listerner;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import ar.com.rockcodes.rockspawnershop.RockSpawnerShop;

 public class Eventos implements Listener {
	private RockSpawnerShop plugin;
	private static final String ChatTAG = ChatColor.GREEN+"["+ChatColor.BLUE+"SPAWNER"+ChatColor.GREEN+"]";

	  public Eventos(RockSpawnerShop instance) {
	       
		  plugin = instance;
		  
	    }
	  
	    @EventHandler
	    public void onPlayerInteract(PlayerInteractEvent e)
	    {
	        
	        if (e.getPlayer().isOp())
	            return;
	        
	        if (e.getAction() != Action.RIGHT_CLICK_BLOCK && e.getAction() != Action.LEFT_CLICK_BLOCK)
	            return;
	        
	        Block b = e.getClickedBlock();
	        
	        if (b == null)
	            return;
	        
	        if (b.getType() != Material.MOB_SPAWNER)
	            return;
	        
	        ItemStack i = e.getItem();
	        
	        if (i == null)
	            return;
	        
	        if (i.getType() != Material.MONSTER_EGG && i.getType() != Material.MONSTER_EGGS)
	            return;
	        
	        CreatureSpawner cs = (CreatureSpawner) b.getState();
	        
	        final Location loc = cs.getLocation();
	        final EntityType type = cs.getSpawnedType();
	        
	        
	        e.setCancelled(true);
	        
	        Bukkit.getScheduler().scheduleSyncDelayedTask(RockSpawnerShop.plugin, new Runnable()
	        {
	            @Override
	            public void run()
	            {
	                Block b = loc.getBlock();
	                
	                if (b == null || b.getType() != Material.MOB_SPAWNER)
	                    return;
	                
	                CreatureSpawner cs = (CreatureSpawner) b.getState();
	                
	                cs.setSpawnedType(type);
	            }
	        });
	    }
	    
	  @EventHandler
	   public  void onBlockplace(final BlockPlaceEvent e) {
		 Block block = e.getBlockPlaced(); 
		 Player player = e.getPlayer() ;
		 	if (block.getType().equals(Material.MOB_SPAWNER)){
		 		if (block.getWorld().getName().equals("Avalon_nether")){
					 player.sendMessage(ChatTAG+"No esta permitido colocar spawners en el nether."); 
					 e.setCancelled(true);
					 return;
		 		}
				ItemStack item = e.getItemInHand();
				ItemMeta istack = item.getItemMeta();
				if (istack.hasLore()){
				String[] a = istack.getLore().get(0).split("-");
				String spawner = a[1]; 
				if (!a[0].equals("Spawner")){player.sendMessage(ChatTAG+"Spawner Invalido"); e.setCancelled(true); }
				else{
					
					int nspawn= 0 ;
					for(BlockFace face : BlockFace.values())
						if(block.getRelative(face,16).getType().equals(Material.MOB_SPAWNER)) {
							 nspawn+= 1;

						 }
					
					/*
					 for (int x = block.getY()-16; x == block.getY()+16; x ++) {
						 for (int y = block.getY()-16; y == block.getY()+16; y ++) {
							 for (int z = block.getY()-16; z == block.getY()+16; z ++) {
						 
								 if(block.getRelative(x, y,z).getType().equals(Material.MOB_SPAWNER)) {
									 nspawn+= 1;

								 }
							 }
							}
						 if (nspawn >= 5){
							 break;
						  }
					 }
                     */
					 if (!(nspawn>= 5)){                             
						 
		 			CreatureSpawner cs = (CreatureSpawner) block.getState();

		 			cs.setSpawnedType(EntityType.fromName(spawner));
		 			player.sendMessage(ChatTAG+"Spawner Seteado a "+spawner);
					 }else{
						 
						 player.sendMessage(ChatTAG+"Se han encotrado mas de 5 spawners alrrededror de 16 bloques a la redonda"); 
						 player.sendMessage(ChatTAG+"Por motivos de calidad no esta permitido esto, buscale otro lugar a tu spawner");
						 e.setCancelled(true); 
					 }
				}
			 
		 	 }
		 	}
		 
		 
	  	}
	  
	  
	  
	
}
