package ar.com.rockcodes.rockspawnershop.Listerner;

import java.util.Arrays;
import java.util.List;

import net.milkbowl.vault.economy.EconomyResponse;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import ar.com.rockcodes.rockspawnershop.RockSpawnerShop;


public class Comandos implements CommandExecutor  {

	private static final String Chatheader = ChatColor.GREEN.BOLD+"_________["+ChatColor.BLUE.BOLD+"SPAWNER"+ChatColor.GREEN.BOLD+"]_________";
	private static final String Chatfooter = ChatColor.GREEN.BOLD+"_________________________";
	private static final String ChatTAG = ChatColor.GREEN+"["+ChatColor.BLUE+"Spawner"+ChatColor.GREEN+"]";
	private RockSpawnerShop plugin;
	public  Comandos(RockSpawnerShop plugin) {
		this.plugin = plugin;
	}
	
	

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("spawner")){
			if (sender instanceof Player) {
				Player player = (Player) sender;
		           if (args.length>0){
		        	   	switch(args[0]) { 
		        	   	case "buy":
		        	   		if (args.length==2){comprarspawn(player,args[1]);}
	        	   			break; // El break corta la ejecucion y no evalua las siguientes
	        	   		case "sell":
	        	   			venderspawn(player);
	        	   			//player.sendMessage("requisitos para el proximo rank");
	        	   			break;
	        	   		case "reload":
	        	   			reloadcnf(player);
	        	   			//player.sendMessage("requisitos para el proximo rank");
	        	   			break;
	        	   		case "spawners":
	        	   			player.sendMessage(Chatheader);
	        	   			sender.sendMessage("SPAWNERS:");
	        	   			for(String a : plugin.config.getConfigurationSection("Spawner").getKeys(false)){
	        	   				if (player.hasPermission("rockspawner.buy."+a)){
	        	   		        sender.sendMessage(ChatColor.RED+a+" "+ChatColor.YELLOW+"- Price:"+ChatColor.RED+plugin.config.getDouble("Spawner."+a+".price"));	
	        	   		        }
	        	   			}
	        	   			player.sendMessage(Chatfooter);
	        	   			//player.sendMessage("requisitos para el proximo rank");
	        	   			break;
	        	   			
		        	   	}return true;
		           }else{
		        	   player.sendMessage(Chatheader);
		        	   sender.sendMessage("Commands list");
		        	   sender.sendMessage(ChatColor.AQUA+"/spawner buy <Spawner>  "+ChatColor.GRAY.ITALIC+" - Buy spawner");   
		        	   sender.sendMessage(ChatColor.AQUA+"/spawner spawners "+ChatColor.GRAY.ITALIC+" - List of spawner that you can buy");
		        	   sender.sendMessage(ChatColor.AQUA+"/spawner sell "+ChatColor.GRAY.ITALIC+" - Sell spawner that you have on hand. 75% refund");
		        	   player.sendMessage(Chatfooter);
		        	   return true;
		           }
				
			    } else {
		           sender.sendMessage(ChatTAG+"You are not a player!");
		           return true;
		        }
			}
		

		return false;
	}
	
	private void reloadcnf(Player player) {
		if (player.isOp()){
			
			
			plugin.config.reloadConfig();
			player.sendMessage(ChatTAG+"Config reloaded!");
			
		}
		
		
	}

	private void venderspawn(Player player) {
		if (!player.hasPermission("rockspawner.sell")){player.sendMessage(ChatTAG+"You don't have permission"); return;}
		ItemStack item = player.getItemInHand();
		if(!item.getType().equals(Material.MOB_SPAWNER)){player.sendMessage(ChatTAG+"That's not a spawner."); return;}
		ItemMeta istack = item.getItemMeta();
		String[] a = istack.getLore().get(0).split("-");
		String spawner = a[1]; 
		if (!a[0].equals("Spawner")){player.sendMessage(ChatTAG+"Invalid spawner"); return;}
		EconomyResponse r = plugin.econ.depositPlayer(player.getName(), plugin.config.getDouble("Spawner."+spawner+".price")*0.75);
    	if(r.transactionSuccess()) {
    		player.getInventory().remove(item);
    		player.sendMessage(ChatTAG+"Spawner sold "+ChatColor.GREEN+plugin.config.getDouble("Spawner."+spawner+".price")*0.75);
    	}
	}

	private void comprarspawn(Player player,String spawner) {
	 spawner = spawner.toLowerCase();
	 if (!player.hasPermission("rockspawner.buy")){player.sendMessage(ChatTAG+"You don't have permission"); return;}
	 if (!player.hasPermission("rockspawner.buy."+spawner)) { player.sendMessage(ChatTAG+"You don't have permission"); return;}
	 if (!plugin.config.contains("Spawner."+spawner)){player.sendMessage(ChatTAG+"Spawner invalido"); return;}
	 if(player.getInventory().firstEmpty()==-1){player.sendMessage(ChatTAG+"You don't have inventory space."); return;}
	 
	 	List<String> lore = Arrays.asList("Spawner-"+spawner);
    	ItemStack is = new ItemStack( Material.MOB_SPAWNER);
    	ItemMeta istack = is.getItemMeta();
    	istack.setDisplayName("Spawner") ;
    	istack.setLore(lore) ;
    	is.setItemMeta(istack) ;
    	if (!(plugin.econ.getBalance(player.getName())>=plugin.config.getDouble("Spawner."+spawner+".price"))){player.sendMessage(ChatTAG+"No tienes suficiente dinero");  return;}
    	EconomyResponse r = plugin.econ.withdrawPlayer(player.getName(), plugin.config.getDouble("Spawner."+spawner+".price"));
    	if(r.transactionSuccess()) {
    		player.getInventory().addItem(is);
    		player.sendMessage(ChatTAG+"Spawner purchased for "+ChatColor.GREEN+plugin.config.getDouble("Spawner."+spawner+".price"));
    	}
       
		
		
	}
	
}
